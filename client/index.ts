const socket = Bun.connect({
    hostname: "localhost",
    port: 8888,
    socket: {
        async data(socket, data) {
          //console.log(`${data}`);
          const receivedData = await buffer2Str(data);
          const lines = receivedData.split("\n");
          
          // Process each line individually
          lines.forEach((line) => {
            // Remove leading and trailing whitespace, including newline characters
            const trimmedLine = line.trim();

            // Ignore lines that consist solely of a newline character
            if (trimmedLine === "") {
              return; // Skip this line
            }

            //console.log(`Received line: ${trimmedLine}`);
            // Process the line as needed
            if (trimmedLine === "TestRecv") {
              console.log("Received TestRecv");
              // Handle the "TestRecv" response here
            } else if (trimmedLine === "ServerData") {
              console.log("Received Server Data");
              // Handle the "ServerData" response here
            } else {
              console.log("Received unexpected data:", trimmedLine);
              // Handle unexpected data here
            }
          });
        },
        open(socket) {
          socket.write("Test\n");
        },
        close(socket) {
          // Handle the socket being closed
        },
        drain(socket) {
          // Handle the socket being ready for more data
        },
        error(socket, error) {
          // Handle socket errors
        },
      
        // client-specific handlers
        connectError(socket, error) {
          console.error("Connection failed:", error);
          // Handle connection failure
        },
        end(socket) {
          // Handle connection closed by the server
        },
        timeout(socket) {
          console.error("Connection timed out.");
          // Handle connection timeout
        },
    },
  });

async function buffer2Str(data: Buffer) : Promise<string> {
    var string = new TextDecoder().decode(data);
    return Promise.resolve(string);
}
