use tokio::net::TcpStream;
use tokio::io::{AsyncReadExt, AsyncWriteExt, BufReader};
use tokio::io::AsyncBufReadExt;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Connect to the server
    let mut stream = TcpStream::connect("localhost:8888").await?;
    let data_to_send = "Test\n"; // Include newline delimiter
    stream.write_all(data_to_send.as_bytes()).await?;
    
    let mut reader = BufReader::new(&mut stream);

    loop {
        let mut line = String::new();
        match reader.read_line(&mut line).await {
            Ok(0) => {
                println!("Server closed the connection.");
                break;
            }
            Ok(10) => {}
            Ok(_) => {
                let line = line.trim_end_matches('\n');
                println!("Received: {}", line);
                // You can add your logic to handle the response here
            }
            Err(e) => {
                eprintln!("Error reading from server: {}", e);
                break;
            }
        }
    }

    Ok(())
}
