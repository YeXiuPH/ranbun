type SocketData = { sessionId: string };

let server = Bun.listen<SocketData>({
    hostname: 'localhost',
    port: 8888,
    socket: {
        async data(socket, data) {
            // console.log(`${data}`);
            socket.write(`${socket.data.sessionId}: ack\n`);
            let response = await processData(data);
            socket.write(`${response}\n`);
        },
        async open(socket) {
            socket.data = { 
                sessionId:  socket.localPort.toString() 
            };
            setInterval(() => {
                const dataToSend = "ServerData\n"; // Include newline delimiter
                socket.write(dataToSend);
            }, 5000);
        },
        async close(socket) {}, // socket closed
        async drain(socket) {}, // socket ready for more data
        async error(socket, error) {}, // error handler
    }
});

async function buffer2Str(data: Buffer) : Promise<string> {
    var string = new TextDecoder().decode(data);
    return Promise.resolve(string);
}

async function processData(data: Buffer): Promise<string> {
    const strBuf = await buffer2Str(data);
    const trimmedData = strBuf.trim();
    console.log(`Recieved Data: ${trimmedData}`);
    switch (trimmedData) {
        case "Test":
            return Promise.resolve("TestRecv");
        default:
            return Promise.resolve("Null");
    }
}